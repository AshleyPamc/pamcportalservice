﻿using RemittanceGenerator.Controller;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for RemittanceService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class RemittanceService : System.Web.Services.WebService
{

    public RemittanceService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public byte[] RemitancePDfDownload(int bureauId, string paidDate, string provId, bool fileType, string claimNo)
    {
        byte[] testByteArray = null;
        string finalfileName = "";

        try
        {

            string path = Server.MapPath("~");
            DirectoryInfo di = new DirectoryInfo(path);
            //  string newFileName = $"{ di }\\assets";
            //Debug.WriteLine(newFileName);
            //if(!di.Exists)
            //{
            //    di.Create();




            //}
            string folderName = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() +
                                DateTime.Now.Day.ToString() + '_' + DateTime.Now.Hour.ToString() +
                                DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

            DirectoryInfo newFolder = new DirectoryInfo(path+"\\assets\\"+folderName);
            newFolder.Create();
            FileInfo savedDataDir = new FileInfo(newFolder.FullName);
            DateTime date = Convert.ToDateTime(paidDate);
            if ((bureauId != 0) && ((provId == null) || (provId == "")))
            {
                PaidClaimsExtract remitanceExtractor = new PaidClaimsExtract(savedDataDir.FullName, bureauId, date, provId, fileType);
                remitanceExtractor.StartProcessing();
            }
            else
            {
                if (((provId != "") && (provId != null)) && (bureauId == 0))
                {
                    PaidClaimsExtract remitanceExtractor = new PaidClaimsExtract(savedDataDir.FullName, bureauId, date, provId, fileType);
                    remitanceExtractor.StartProcessing();
                }
                else
                {
                    PaidClaimsExtract remitanceExtractor = new PaidClaimsExtract(savedDataDir.FullName, claimNo, fileType);
                    remitanceExtractor.StartProcessing();
                }

            }

            //  Debug.WriteLine(f.Exists.ToString());
            string day = date.Day.ToString();
            if (day.Length == 1)
            {
                day = "0" + day;
            }
            string month = date.Month.ToString();
            if (month.Length == 1)
            {
                month = "0" + month;
            }
            string year = date.Year.ToString();
            string tempdate = year + month + day;
            if ((bureauId != 0) && ((provId == null) || (provId == "")))
            {
                if (fileType)
                {
                    finalfileName = savedDataDir.FullName+"\\PaidClaims__"+tempdate+"_Era.txt";
                }
                else
                {
                    finalfileName = savedDataDir.FullName+"\\Remits__"+tempdate+".zip";
                }
            }
            if ((bureauId == 0) && ((provId != null) || (provId != "")))
            {
                if (fileType)
                {
                    finalfileName = savedDataDir.FullName+"\\PaidClaims_"+provId+"_"+tempdate+"_Era.txt";
                }
                else
                {
                    finalfileName = savedDataDir.FullName+"\\Remits_"+provId+"_"+tempdate+".zip";
                }


            }

            if ((bureauId == 0) && ((provId == null) || (provId == "")))
            {
                if (fileType)
                {
                    finalfileName = savedDataDir.FullName+"\\PaidClaims__00010101_Era.txt";
                }
                else
                {
                    finalfileName = savedDataDir.FullName+"\\Rem_"+claimNo+".zip";
                }
            }


            FileInfo newFile = new FileInfo(finalfileName);
            testByteArray = File.ReadAllBytes(newFile.FullName);
            Debug.WriteLine(testByteArray.Length);
        }
        catch (Exception e)
        {

            Debug.WriteLine(e.StackTrace);
            throw;
        }
        Debug.Write(testByteArray);
        return testByteArray;
    }

    [WebMethod]
    public byte[] RemitanceClientClaimDownload(bool fileType, string claimNo, bool membProv)
    {
        byte[] returnedArray;

        string finalfileName = "";

        try
        {
            string path = Server.MapPath("~");
            DirectoryInfo di = new DirectoryInfo(path);
            string folderName = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() +
                                DateTime.Now.Day.ToString() + '_' + DateTime.Now.Hour.ToString() +
                                DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

            DirectoryInfo newFolder = new DirectoryInfo(path+"\\assets\\"+folderName);
            newFolder.Create();
            FileInfo savedDataDir = new FileInfo(newFolder.FullName);

            PaidClaimsExtract remitanceExtractor = new PaidClaimsExtract(savedDataDir.FullName, claimNo, membProv, fileType);
            remitanceExtractor.StartProcessing();

            if (fileType == false)
            {
                finalfileName = savedDataDir.FullName+"\\Rem_"+claimNo+".zip";
            }
            else
            {
                finalfileName = savedDataDir.FullName+"\\PaidClaims__00010101_Era.txt";
            }


            FileInfo newFile = new FileInfo(finalfileName);
            returnedArray = File.ReadAllBytes(newFile.FullName);
            Debug.WriteLine(returnedArray.Length);
        }
        catch (Exception e)
        {
            var msg = e.Message;
            Debug.WriteLine(e.StackTrace);
            throw;
        }

        return returnedArray;
    }

}
