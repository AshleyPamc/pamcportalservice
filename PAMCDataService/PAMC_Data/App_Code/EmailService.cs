﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for EmailService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class EmailService : System.Web.Services.WebService
{

    public EmailService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public bool SendEmail(string body, string subject, string toAddress)
    {
        bool sended = false;
        string[] emailList;

        try
        {
            emailList = new string[toAddress.Split(';').Length];
            emailList = toAddress.Split(';');

            using (var message = new MailMessage())
            {
                foreach (string addr in emailList)
                {
                    message.To.Add(new MailAddress(addr));
                }
                //message.To.Add(new MailAddress(toAddress));
                message.From = new MailAddress("Servicerequest@pamc.co.za", "Portal notification");
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = false;

                using (var client = new SmtpClient("192.168.16.1"))
                {
                    client.Host = "192.168.16.1";
                    client.UseDefaultCredentials = false;
                    client.Port = 465;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.Credentials = new NetworkCredential("Servicerequest", "12345678");
                    ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                    client.EnableSsl = true;
                    client.Send(message);
                    sended = true;
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.Message;
            return sended;
        }

        return sended;
    }

    [WebMethod]
    public bool SendMeetingInvite(string body, string subject, string toAddress,string date)
    {
        bool sended = false;
        string[] emailList;


        try
        {

            string from = (date + " 10:00:00").Replace(":", "").Replace("/", "").Replace("-", "").Replace(" ", "");
            string to = (date + " 10:30:00").Replace(":", "").Replace("/", "").Replace("-", "").Replace(" ", "");
            DateTime fromdt = DateTime.ParseExact(from, "yyyyMMddHHmmss", null);
            DateTime todt = DateTime.ParseExact(to, "yyyyMMddHHmmss", null);

            string location = "";

            DateTime stamp = fromdt;
            emailList = new string[toAddress.Split(';').Length];
            emailList = toAddress.Split(';');


            using (var message = new MailMessage())
            {
                foreach (string addr in emailList)
                {
                    message.To.Add(new MailAddress(addr));
                }
                //message.To.Add(new MailAddress(toAddress));
                message.From = new MailAddress("Servicerequest@pamc.co.za", "Portal notification");
                message.Subject = subject;
                message.Body = body;
                message.IsBodyHtml = false;

                message.Headers.Add("Content-class", "urn:content-classes:calendarmessage");
                StringBuilder str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");
                str.AppendLine("PRODID:-//PAMC//PAMC//ZA");
                str.AppendLine("VERSION:2.0");
                str.AppendLine("METHOD:REQUEST");
                str.AppendLine("BEGIN:VEVENT");
                str.AppendLine(string.Format("DTSTART:{0}", fromdt.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z")));
                str.AppendLine(string.Format("DTSTAMP:{0}", stamp.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z")));
                str.AppendLine(string.Format("DTEND:{0}", todt.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z")));
                str.AppendLine(string.Format("LOCATION:{0}", location));
                str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
                str.AppendLine(string.Format("DESCRIPTION:{0}", message.Body));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", message.Body));
                str.AppendLine(string.Format("SUMMARY:{0}", message.Subject));
                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", message.From.Address));

                str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", message.To[0].DisplayName, message.To[0].Address));

                str.AppendLine("BEGIN:VALARM");
                str.AppendLine("TRIGGER:-PT15M");
                str.AppendLine("ACTION:DISPLAY");
                str.AppendLine("DESCRIPTION:Reminder");
                str.AppendLine("END:VALARM");
                str.AppendLine("END:VEVENT");
                str.AppendLine("END:VCALENDAR");

                using (var client = new SmtpClient("192.168.16.1"))
                {
                    System.Net.Mime.ContentType contype = new System.Net.Mime.ContentType("text/calendar");
                    contype.Parameters.Add("method", "REQUEST");
                    contype.Parameters.Add("name", "Meeting.ics");
                    AlternateView avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), contype);
                    message.AlternateViews.Add(avCal);
                    client.Host = "192.168.16.1";
                    client.UseDefaultCredentials = false;
                    client.Port = 465;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.Credentials = new NetworkCredential("Servicerequest", "12345678");
                    ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                    client.EnableSsl = true;
                    client.Send(message);
                    sended = true;
                }
            }
        }
        catch (Exception e)
        {
            var msg = e.Message;
            return sended;
        }

        return sended;
    }
}
