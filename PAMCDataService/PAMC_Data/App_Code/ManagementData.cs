﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
/// <summary>
/// Summary description for ManagementData
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class ManagementData : System.Web.Services.WebService
{

    public ManagementData()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    public List<ClaimsSummary> ServiceDateData(string HPCode)
    {

        DataTable dt = new DataTable();
        SqlCommand cmd = new SqlCommand();
        SqlConnection cn = new SqlConnection();
        cn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DRC_DWConnectionString"].ConnectionString;
        string[] hpcodes;

        if (cn.State != ConnectionState.Open)
        {
            cn.Open();
        }
        cmd.Connection = cn;
        cmd.CommandTimeout = 3600;

        if (HPCode.Contains(';'))
        {
            hpcodes = new string[HPCode.Split(';').Length];
            hpcodes = HPCode.Split(';');
            for (int i = 0; i < hpcodes.Length; i++)
            {
                string sql = "SELECT        YEAR(DATERECD) AS ReceivedYear, MONTH(DATEPAID) AS ReceivedMonth, YEAR(DATEPAID) AS PaidYear, MONTH(DATEPAID) AS PaidMonth, \n"
           + "YEAR(FROMDATESVC) AS ServiceYear, MONTH(FROMDATESVC) AS ServiceMonth, \n"
           + "                         HPCODE, OPT, isRiskOpt, SUM(NET) AS NET,SUM(BILLED) AS BILLED ,STATUS,\n"
           + "						 convert(varchar,YEAR(FROMDATESVC)) + '-'  + right('00'+convert(varchar,MONTH(FROMDATESVC)),2) AS ServiceYM\n"
           + "FROM            FactClaims\n"
           + "WHERE        (FROMDATESVC >= DATEADD(MONTH,- 12,GETDATE())) AND isRiskOpt = 'NonRisk'\n"
           + "GROUP BY YEAR(FROMDATESVC), MONTH(FROMDATESVC), HPCODE, OPT, isRiskOpt, YEAR(DATEPAID), MONTH(DATEPAID), YEAR(DATERECD), MONTH(DATERECD), STATUS\n"
           + "HAVING       (HPCODE = '" + hpcodes[i] + "' )\n"
           + "order by 13";
                //       string sql = "SELECT        YEAR(cm.DATERECD) AS ReceivedYear, MONTH(cm.DATEPAID) AS ReceivedMonth, YEAR(cm.DATEPAID) AS PaidYear, MONTH(cm.DATEPAID) AS PaidMonth, YEAR(cd.FROMDATESVC) AS ServiceYear, \n"
                //+ "                         MONTH(cd.FROMDATESVC) AS ServiceMonth, cm.HPCODE, cm.OPT, 0 AS isRiskOpt, SUM(cd.NET) AS NET, SUM(cd.BILLED) AS BILLED, cm.STATUS, CONVERT(varchar, YEAR(cd.FROMDATESVC)) \n"
                //+ "                         + '-' + RIGHT('00' + CONVERT(varchar, MONTH(cd.FROMDATESVC)), 2) AS ServiceYM\n"
                //+ "FROM            CLAIM_MASTERS AS cm INNER JOIN\n"
                //+ "                         CLAIM_DETAILS AS cd ON cm.CLAIMNO = cd.CLAIMNO\n"
                //+ "WHERE        (YEAR(cd.FROMDATESVC) >= YEAR(GETDATE()) - 2)\n"
                //+ "GROUP BY YEAR(cd.FROMDATESVC), MONTH(cd.FROMDATESVC), cm.HPCODE, cm.OPT, YEAR(cm.DATEPAID), MONTH(cm.DATEPAID), YEAR(cm.DATERECD), MONTH(cm.DATERECD), cm.STATUS\n"
                //+ "HAVING        (cm.HPCODE = '"+ hpcodes[i]+"' )\n"
                //+ "ORDER BY ServiceYM";

                cmd.CommandText = sql;

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    dt.Load(dr);
                }

            }
        }
        else
        {
            hpcodes = new string[1];
            hpcodes[0] = HPCode;

            string sql = "SELECT        YEAR(DATERECD) AS ReceivedYear, MONTH(DATEPAID) AS ReceivedMonth, YEAR(DATEPAID) AS PaidYear, MONTH(DATEPAID) AS PaidMonth, \n"
+ "YEAR(FROMDATESVC) AS ServiceYear, MONTH(FROMDATESVC) AS ServiceMonth, \n"
+ "                         HPCODE, OPT, isRiskOpt, SUM(NET) AS NET,SUM(BILLED) AS BILLED ,STATUS,\n"
+ "						 convert(varchar,YEAR(FROMDATESVC)) + '-'  + right('00'+convert(varchar,MONTH(FROMDATESVC)),2) AS ServiceYM\n"
+ "FROM            FactClaims\n"
+ "WHERE        (FROMDATESVC >= DATEADD(MONTH,- 12,GETDATE())) AND isRiskOpt = 'NonRisk'\n"
+ "GROUP BY YEAR(FROMDATESVC), MONTH(FROMDATESVC), HPCODE, OPT, isRiskOpt, YEAR(DATEPAID), MONTH(DATEPAID), YEAR(DATERECD), MONTH(DATERECD), STATUS\n"
+ "HAVING       (HPCODE = '" + hpcodes[0] + "' )\n"
+ "order by 13";

            //   string sql = "SELECT        YEAR(cm.DATERECD) AS ReceivedYear, MONTH(cm.DATEPAID) AS ReceivedMonth, YEAR(cm.DATEPAID) AS PaidYear, MONTH(cm.DATEPAID) AS PaidMonth, YEAR(cd.FROMDATESVC) AS ServiceYear, \n"
            //+ "                         MONTH(cd.FROMDATESVC) AS ServiceMonth, cm.HPCODE, cm.OPT, 0 AS isRiskOpt, SUM(cd.NET) AS NET, SUM(cd.BILLED) AS BILLED, cm.STATUS, CONVERT(varchar, YEAR(cd.FROMDATESVC)) \n"
            //+ "                         + '-' + RIGHT('00' + CONVERT(varchar, MONTH(cd.FROMDATESVC)), 2) AS ServiceYM\n"
            //+ "FROM            CLAIM_MASTERS AS cm INNER JOIN\n"
            //+ "                         CLAIM_DETAILS AS cd ON cm.CLAIMNO = cd.CLAIMNO\n"
            //+ "WHERE        (YEAR(cd.FROMDATESVC) >= YEAR(GETDATE()) - 2)\n"
            //+ "GROUP BY YEAR(cd.FROMDATESVC), MONTH(cd.FROMDATESVC), cm.HPCODE, cm.OPT, YEAR(cm.DATEPAID), MONTH(cm.DATEPAID), YEAR(cm.DATERECD), MONTH(cm.DATERECD), cm.STATUS\n"
            //+ "HAVING        (cm.HPCODE = '" + hpcodes[0] + "' )\n"
            //+ "ORDER BY ServiceYM";
            //   cmd.CommandText = sql;
            cmd.CommandText = sql;
            using (SqlDataReader dr = cmd.ExecuteReader())
            {
                dt.Load(dr);
            }

        }




        List<ClaimsSummary> report = new List<ClaimsSummary>();
        foreach (DataRow item in dt.Rows)
        {
            ClaimsSummary r = new ClaimsSummary();
            r.Net = Convert.ToDecimal(item["NET"].ToString());
            r.Hpcode = item["HPCODE"].ToString();
            r.Billed = 0;
            r.Opt = item["OPT"].ToString();
            r.IsRiskOpt = item["isRiskOpt"].ToString();
            if (!DBNull.Value.Equals(item["ServiceYear"]))
            {
                r.ServiceYear = Convert.ToDecimal(item["ServiceYear"]);
            }
            else
            {
                r.ServiceYear = 0;
            }
            if (!DBNull.Value.Equals(item["ServiceYear"]))
            {
                r.ServiceMonth = Convert.ToDecimal(item["ServiceMonth"]);
            }
            else
            {
                r.ServiceMonth = 0;
            }
            if (!DBNull.Value.Equals(item["PaidYear"]))
            {
                r.PaidYear = Convert.ToDecimal(item["PaidYear"]);
            }
            else
            {
                r.PaidYear = 0;
            }
            if (!DBNull.Value.Equals(item["PaidMonth"]))
            {
                r.PaidMonth = Convert.ToDecimal(item["PaidMonth"]);
            }
            else
            {
                r.PaidMonth = 0;
            }
            if (!DBNull.Value.Equals(item["ReceivedYear"]))
            {
                r.ReceivedYear = Convert.ToDecimal(item["ReceivedYear"]);
            }
            else
            {
                r.ReceivedYear = 0;
            }
            if (!DBNull.Value.Equals(item["ReceivedMonth"]))
            {
                r.ReceivedMonth = Convert.ToDecimal(item["ReceivedMonth"]);
            }
            else
            {
                r.ReceivedMonth = 0;
            }
            r.Status = item["STATUS"].ToString();
            r.Billed = Convert.ToDecimal(item["NET"]);
            r.ServiceYM = item["ServiceYM"].ToString();
            report.Add(r);
        }
        return report;



    }

}
