﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for IncidentsReport
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class IncidentsReport : System.Web.Services.WebService
{

    public IncidentsReport()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    public byte[] GetIncidentsReport(string date)
    {
        byte[] returendArray;

        string path = Server.MapPath("~");

        DirectoryInfo dd = new DirectoryInfo(path + "\\assets\\IncidentsReport\\");
        FileInfo savedDataDir = new FileInfo(path+"\\assets\\IncidentsReport\\Report.xlsx");

        DataTable dt = new DataTable();
        SqlConnection cn = new SqlConnection();

        SqlCommand cmd = new SqlCommand();

        cn.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DRCConnectioString2"].ConnectionString;

        if(cn.State != ConnectionState.Open)
        {
            cn.Open();
        }
        cmd.Connection = cn;

        string sql = "SELECT DISTINCT \n"
                   + "                         CSM_MASTERS.CUSTNAME AS [CONTACT NAME], CSM_MASTERS.CSINO, CSM_CONTACTTYPES.DESCR AS CUSTCONTACTTYPE, CSM_MASTERS.CUSTEMAIL, CSM_MASTERS.INSTITUTION AS CUSTINSTITUTION, \n"
                   + "                         CASE WHEN len(ltrim(ISNULL(CSM_MASTERS.BROKERNAME, ''))) > 0 THEN ISNULL(CSM_MASTERS.BROKERNAME, '') ELSE ISNULL(CSM_MASTERS.BUREAUNAME, '') END AS CUSTINSTITNAME, \n"
                   + "                         CSM_MASTERS.CUSTCATEGORY, CSM_SUBTYPES.DESCR, CSM_TYPES.DESCR AS INCTYPEDESCRIPTION, CSM_DETAILS.USERNAME, CONVERT(varchar, CSM_DETAILS.NOTES) AS notes, CSM_MASTERS.CLIENTTYPE,\n"
                   + "						 CSM_MASTERS.CREATEDATE\n"
                   + "FROM            CSM_MASTERS INNER JOIN\n"
                   + "                         CSM_DETAILS ON CSM_MASTERS.CSINO = CSM_DETAILS.CSINO AND CSM_MASTERS.CSINO = CSM_DETAILS.CSINO INNER JOIN\n"
                   + "                         CSM_SUBTYPES ON CSM_MASTERS.SUBTYPE = CSM_SUBTYPES.SUBTYPE AND CSM_MASTERS.SUBTYPE = CSM_SUBTYPES.SUBTYPE AND CSM_MASTERS.INCTYPE = CSM_SUBTYPES.INCTYPE INNER JOIN\n"
                   + "                         CSM_TYPES ON CSM_MASTERS.INCTYPE = CSM_TYPES.INCTYPE AND CSM_MASTERS.INCTYPE = CSM_TYPES.INCTYPE INNER JOIN\n"
                   + "                         CSM_CONTACTTYPES ON CSM_MASTERS.CONTACT = CSM_CONTACTTYPES.CONTACT AND CSM_MASTERS.CONTACT = CSM_CONTACTTYPES.CONTACT\n"
                   + "WHERE        (CSM_MASTERS.CREATEDATE >= '"+date+"') AND (CSM_MASTERS.STATUS = 'C') and CSM_CONTACTTYPES.DESCR = 'E-MAIL' and USERNAME in ('martha','VERUSHKA','CAITLIN','JANICEB','DANIELLE', 'MARYKE','MICHELLE','sophy','montie')\n"
                   + "and charindex('@',CUSTEMAIL,1) > 0";
        cmd.CommandText = sql;

        using(SqlDataReader dr = cmd.ExecuteReader())
        {
            dt.Load(dr);
        }

        int totalcols = 0;
        using(ExcelPackage package = new ExcelPackage(savedDataDir))
        {
            ExcelWorksheet worksheet = package.Workbook.Worksheets["incidents"];
            int cols = 1;
            int rows = 2;
            int startRow = worksheet.Dimension.Start.Row + 1;
            int endRow = worksheet.Dimension.End.Row;
            int startCol = worksheet.Dimension.Start.Column;
            int endCol = worksheet.Dimension.End.Column;
            for (int i = startRow; i <= endRow; i++)
            {
                for (int j = startCol; j <= endCol; j++)
                {
                    worksheet.Cells[i, j].Clear();
                }
            }

            foreach (DataRow row in dt.Rows)
            {

                worksheet.Cells[rows, cols].Value = row["CONTACT NAME"].ToString();
                cols++;
                worksheet.Cells[rows, cols].Value = row["CSINO"].ToString();
                cols++;
                worksheet.Cells[rows, cols].Value = row["CUSTCONTACTTYPE"].ToString();
                cols++;
                worksheet.Cells[rows, cols].Value = row["CUSTEMAIL"].ToString();
                cols++;
                worksheet.Cells[rows, cols].Value = row["CUSTINSTITUTION"].ToString();
                cols++;
                worksheet.Cells[rows, cols].Value = row["CUSTINSTITNAME"].ToString();
                cols++;
                worksheet.Cells[rows, cols].Value = row["CUSTCATEGORY"].ToString();
                cols++;
                worksheet.Cells[rows, cols].Value = row["DESCR"].ToString();
                cols++;
                worksheet.Cells[rows, cols].Value = row["INCTYPEDESCRIPTION"].ToString();
                cols++;
                worksheet.Cells[rows, cols].Value = row["USERNAME"].ToString();
                cols++;
                worksheet.Cells[rows, cols].Value = row["CLIENTTYPE"].ToString();
                cols++;
                worksheet.Cells[rows, cols].Value = row["CREATEDATE"].ToString();
                cols++;
                worksheet.Cells[rows, cols].Value = row["notes"].ToString();
                totalcols = cols;
                cols =1;
                rows++;

            }

            worksheet.Cells.AutoFitColumns();

            string beginAdd = worksheet.Cells[1, 1].Address;
            string endAdd = worksheet.Cells[1, totalcols].Address;

            worksheet.Cells[beginAdd + ":" + endAdd].AutoFilter = true;
                



            int r = dd.GetFiles().Length - 1;
            string newFileName = "INCIDENTS_"+DateTime.Now.ToString("yyyyMMdd") + "_" + date.Replace("/", "").Replace("-", "")+"_"+r.ToString()+".xlsx";

            FileInfo ff = new FileInfo(path + "\\assets\\IncidentsReport\\" + newFileName);
            package.SaveAs(ff);

            returendArray = File.ReadAllBytes(ff.FullName);
        }



        return returendArray;
    }
}
